package com.ankir33gmail.reciclerviewlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RecyclerAdapter.UserClickListener {

    RecyclerView recyclerView;
    ArrayList<User> list = new ArrayList<>();
    public static final String KEY = "user";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        list = createList();
        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(this, list, this);
        recyclerView.setAdapter(recyclerAdapter);


    }

    @Override
    public void onClick(User user) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(KEY, user);
        startActivity(intent);
    }

    ArrayList<User> createList() {
        ArrayList<User> rezult = new ArrayList<>();
        for (int i = 1; i < 15; i++) {
            User user = new User();
            user.name = "Имя " + i;
            user.email = i + "@gmail.com";
            user.ikonka = R.drawable.pinkhellokitty;
            user.adress = "Харьков" + i + i;
            user.phone = "050-123-45-67";
            rezult.add(user);
        }
        return rezult;
    }


}
