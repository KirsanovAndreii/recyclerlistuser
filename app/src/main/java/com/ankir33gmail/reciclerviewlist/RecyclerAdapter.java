package com.ankir33gmail.reciclerviewlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by AnKir on 12.05.2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    interface ItemClickListener {
        void onClick(int position);
    }

    interface UserClickListener {
        void onClick(User user);
    }


    ArrayList<User> list;
    Context context;
    private UserClickListener userClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            userClickListener.onClick(list.get(position));
        }
    };

    public RecyclerAdapter(Context context, ArrayList<User> list, UserClickListener userClickListener) {
        this.list = list;
        this.context = context;
        this.userClickListener = userClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false);

        return new ViewHolder(view, itemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = list.get(position);
        holder.textViewName.setText(user.name);
        holder.textViewEmail.setText(user.email);
        holder.imageViewIconca.setImageResource(user.ikonka);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName;
        public TextView textViewEmail;
        public ImageView imageViewIconca;

        public ViewHolder(View itemView, final ItemClickListener itemClickListener) {
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.text_view_name);
            textViewEmail = (TextView) itemView.findViewById(R.id.text_view_email);
            imageViewIconca = (ImageView) itemView.findViewById(R.id.iconca);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }


}
