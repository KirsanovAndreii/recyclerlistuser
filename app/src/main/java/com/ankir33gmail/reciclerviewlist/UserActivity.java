package com.ankir33gmail.reciclerviewlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import static com.ankir33gmail.reciclerviewlist.MainActivity.KEY;

public class UserActivity extends AppCompatActivity {

    TextView textViewName;
    TextView textViewEmail;
    TextView textViewAdress;
    TextView textViewPhone;
    ImageView img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        textViewName = (TextView) findViewById(R.id.textView1);
        textViewEmail = (TextView) findViewById(R.id.textView2);
        textViewAdress = (TextView) findViewById(R.id.textView3);
        textViewPhone = (TextView) findViewById(R.id.textView4);
        img = (ImageView) findViewById(R.id.img_view5);


        if (getIntent() != null) {
            User user = (User) getIntent().getSerializableExtra(KEY);
            textViewName.setText(user.name);
            textViewEmail.setText(user.email);
            textViewAdress.setText(user.adress);
            textViewPhone.setText(user.phone);
            img.setImageResource(user.ikonka);

        }
    }
}
